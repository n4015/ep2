import numpy as np

def transformacaoLinear(lims = np.ndarray):                                                         #Gera a equação linear que transporta os pontos [-1; 1] para [a, b]
    
    if len(lims) != 2:                             #Checa se o vetor lims tem dimensão correta
        raise Exception('Erro: vetor dos limites de integração está incorreto; deve ser um vetor 1x2')

    a = lims[0]
    b = lims[1]

    transfLin = np.array([(b-a)/2, (b+a)/2])      #Cria uma array com os coeficientes da transformação linear {T(x) = x * (b-a)/2 + (b+a)/2}

    return transfLin

def nos(n):                                                                                         #Retorna os nós e pesos de acordo com o número de nós
    
    nos ={                                         #Dicionário que armazena os pesos e nós de acordo com o número de nós  (np.array([[nós], 
                                                   #                                                                                 [pesos]]))
    "3" : np.array([[0, np.sqrt(0.6)],
                    [8/9, 5/9]]),
    
    "6": np.array([[0.2386191860831969086305017, 0.6612093864662645136613996, 0.9324695142031520278123016], 
                   [0.4679139345726910473898703, 0.3607615730481386075698335, 0.1713244923791703450402961]]),

    "8": np.array([[0.1834346424956498049394761, 0.5255324099163289858177390, 0.7966664774136267395915539, 0.9602898564975362316835609],
                   [0.3626837833783619829651504, 0.3137066458778872873379622, 0.2223810344533744705443560, 0.1012285362903762591525314]]),

    "10": np.array([[0.1488743389816312108848260, 0.4333953941292471907992659, 0.6794095682990244062343274, 0.8650633666889845107320967, 0.9739065285171717200779640],
                    [0.2955242247147528701738930, 0.2692667193099963550912269, 0.2190863625159820439955349, 0.1494513491505805931457763, 0.0666713443086881375935688]])
}
    
    if type(nos.get(n)) == type(None):             #Chega se o n passado é correspndente a um dos ns presentes no dicionário
        raise Exception("Número de nós inválido")

    else:                                          #Gera dois vetores com os pesos e nós a a partir do dicionário
        w = nos.get(n)[1]
        x = nos.get(n)[0]

    return w, x

def integralGauss(f, lims=np.ndarray, w=np.ndarray, x=np.ndarray):                                  #Calcula a integral pelo método de Gauss

    coefTransLin = transformacaoLinear(lims)                            #Determina os coeficeintes da transfomação linear dos limites
    transLin = lambda x: x*coefTransLin[0] + coefTransLin[1]            #Constroi função lambda da transfomação linear dos limites

    integ = 0                                                           #Variável que armazena os valores do somatório
    for i in range(len(w)):                                             #Calcula para cada nó transportado linearmente a parcela do somatório correspondente
        if x[i] == 0:
            integ += w[i]*f(transLin(x[i]))
        else:                                                           #Verifica se o nó é igual a zero, caso contrario calcula a parcela correspondente aos nós xi e -xi
            integ += w[i]*(f(transLin(x[i])) + f(transLin(-x[i])))
            
    integ = coefTransLin[0]*integ                                      #Multiplica o somatório final pelo coeficiente angular da tranformação
    return integ

def integralDuplaGauss(f, limx=np.ndarray, f_limy=np.ndarray, w=np.ndarray, x=np.ndarray):          #Calcula integral dupla pelo método de Gauss
    
    #f deve ser uma função f(x,y) // limx deve ser uma np.array de 2 números // limy deve ser uma np.array de duas funções lambda ( c(x) e d(x) ) 
    
    coefTransLinx = transformacaoLinear(limx)                                               #Determina os coeficeintes da transfomação linear dos limites de x
    transLinx = lambda x: coefTransLinx[0]*x + coefTransLinx[1]                             #Constroi função lambda da transfomação linear dos limites de x

    integ = 0                                                                               #Variável que armazena os valores do somatório
    for i in range(len(w)):
        limy = np.array([f_limy[0](transLinx(x[i])), f_limy[1](transLinx(x[i]))])           #Calcula para cada nó de x os limties correspondentes de y   
        g = lambda y: f(transLinx(x[i]), y)                                                 #Gera a função correspondente para cada valor de x ( f(xi, y) )
        integ += w[i] * integralGauss(g, limy, w, x)                                        #Calcula o somatório para todos os nós de y

        if x[i] != 0:                                                                       #Verifica se o nó de x é diferente de zero, caso positivo refaz o processo anterior para -xi
            limy = np.array([f_limy[0](transLinx(-x[i])), f_limy[1](transLinx(-x[i]))])
            g = lambda y: f(transLinx(-x[i]), y)
            integ += w[i] * integralGauss(g, limy, w, x)

    integ = integ*coefTransLinx[0]                                                          #Multiplica os valor final dos somatórios pelo coeficiente angular da tranformação de x

    return integ