# Autores: 
# --------- Eduardo Pascual 11804251 ---------
# --------- Luiza Trindade  11884709 ---------

from numbers import Integral
import numpy as np 
import functions_Integral

def teste1(n, w, x):
        # Volume do Cubo de aresta unitária
        f = lambda x,y: 1
        limx = np.array([0,1])
        limy = np.array([lambda x: 0, lambda x: 1])
        cubo = functions_Integral.integralDuplaGauss(f, limx, limy, w, x)

        # Volume do Tetraedro reto de aresta unitária
        f = lambda x,y: 1-x-y
        limx = np.array([0,1])
        limy = np.array([lambda x: 0, lambda x: 1-x ])
        tetraedro = functions_Integral.integralDuplaGauss(f,limx, limy, w, x)

        print(f"\n O Volume do cubo de aresta 1 com {n} nós: {cubo}. O valor exato é de 1 \n")
        print(f" O volume do tetraedro com {n} nós: {tetraedro}. O valor exato é de 1/6 ou {1/6}\n ")

def teste2(n, w, x):
    # Área da região
    f = lambda x,y: 1
    limx = np.array([0,1])
    limy = np.array([lambda x: 0, lambda x: 1-x**2])
    areax = functions_Integral.integralDuplaGauss(f, limx, limy, w, x)

    f = lambda x,y: 1
    limy = np.array([0,1])
    limx = np.array([lambda y: 0, lambda y: np.sqrt(1-y)])
    areay = functions_Integral.integralDuplaGauss(f, limy, limx, w, x)

    print(f"\n Com {n} nós, integrando primeiramente em y, a área resulta em: {areax}. O valor exato é de 2/3 ou {2/3}")
    print(f"\n Com {n} nós, integrando primeiramente me x, a área resulta em: {areay}. O valor exato é de 2/3 ou {2/3}")

def teste3(n, w, x):
    #Área da superfície
    f = lambda x,y: np.sqrt(-y**2/x**4 * np.exp(2*y/x) + 1/x**2 * np.exp(2*y/x) +1)
    limx = np.array([0.1,0.5])
    limy = np.array([lambda x: x**3, lambda x: x**2])
    area = functions_Integral.integralDuplaGauss(f, limx, limy, w, x)

    print(f"\n Com {n} nós, a área da superfície gerada pela função é igual a: {area}")

def teste4(n, w, x):
    #Área da calota com revolução em torno do eixo X
    f = lambda x,y: y
    limx = np.array([0,1/4])
    limy = np.array([lambda x: 0, lambda x: np.sqrt(1-x**2)])
    calota = 2*np.pi*functions_Integral.integralDuplaGauss(f, limx, limy, w, x)

    #Área do sólido com revolução em torno do eixo y
    f = lambda y, x: x
    limy = np.array([-1, 1])
    limx = np.array([lambda xy: 0, lambda y: np.exp(-y**2)])
    rev = 2*np.pi*functions_Integral.integralDuplaGauss(f, limy, limx, w, x)

    print(f"\n Com {n} nós, o volume da calota é igual a: {calota}.")
    print(f"\n Com {n} nós, o volume do sólido de revolução é igual a: {rev}.")

def main():
    teste = input("Número do Teste: ")                                      #Seleciona o teste de acordo com a resposta do usuário

    if teste != "1" and teste != "2" and teste != "3" and teste != "4":     #Checa se o número do teste é válido
        raise Exception("Teste inválido")

    n = input("Número de nós que deseja-se utilizar (3, 6, 8 ou 10): ")     #Seleciona os nós e pésos de acordo com a resposta do usuário

    w, x = functions_Integral.nos(n)

    #Roda o teste correspondente

    if teste == "1":
        teste1(n,w,x)
    
    elif teste == "2":
        teste2(n, w, x)
    
    elif teste == "3":
        teste3(n, w, x)
        pass
    elif teste == "4":
        teste4(n, w, x)

if __name__ == "__main__":
    main()