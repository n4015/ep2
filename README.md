# -- Versões -- #
Progrmado em: Python 3.8.0
Bicliotecas utilizadas: Numpy 1.21.1


# -- Instruções de utilização do programa -- #

Ao rodar a função 'main' o usuário deve escolher o teste que quer rodar (1,2,3 ou 4). Em seguida deve escolher o número de nós que quer utilizar para resolver as integrais (3,6,8 ou 10).
O resultado de cada teste então é impresso no monitor.
